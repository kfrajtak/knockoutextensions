define([
    'knockout'
], function(ko) {

    ko.bindingHandlers.content = {

        'init': function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = ko.utils.unwrapObservable(valueAccessor());

            var templateNameFunc = function(value, itemBindingContext)
            {
                if('template' in options)
                    return options['template'];

                if(!value['type'])
                {
                    throw new Error("Set 'type' of view model class.");
                }

                var type = value.type;
                if(typeof type == 'function')
                    type = type();

                return type.replace(/viewmodel/ig, 'View');
            };

            if ('foreach' in options) {
                var dataArray = (options['foreach']) || [];
                ko.renderTemplateForEach(templateNameFunc, dataArray, options, element, bindingContext);
                return;
            }

            var templateName = templateNameFunc(options.data),
                dataValue = ko.utils.unwrapObservable(options['data']);

            var innerBindingContext = bindingContext['createChildContext'](dataValue, options['as']);
            ko.renderTemplate(templateName || element, innerBindingContext, options, element);
        }
    };
});